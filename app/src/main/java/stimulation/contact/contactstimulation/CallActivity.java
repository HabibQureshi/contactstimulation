package stimulation.contact.contactstimulation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import stimulation.contact.contactstimulation.Fragments.Call;

public class CallActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        Call call= new Call();                  // displaying call fragment
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame, call,"callFragment")
                .addToBackStack(null)
                .commit();
    }
    public void cancelCall(){   // cancel call and go back to main screen
        android.support.v4.app.Fragment fragment = getSupportFragmentManager().findFragmentByTag("callFragment");
        if(fragment != null)
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        startActivity(new Intent(this,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    @Override
    public void onBackPressed() {
    }
}
