package stimulation.contact.contactstimulation.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import stimulation.contact.contactstimulation.Contact;
import stimulation.contact.contactstimulation.MainActivity;


public class Sql extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "contact";
    private static final String COL_ID ="id";
    private static final String COL_NAME ="name";
    private static final String COL_ADDRESS = "address";
    private static final String COL_CONTACT ="contact";
    private static final String selectQuery = "SELECT  * FROM " + TABLE_NAME + " ORDER BY " +
            COL_NAME;
    private MainActivity activity;


    public static final String CREATE_TABLE =
            "CREATE TABLE "+TABLE_NAME+" ("
                    +COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"
                    +COL_NAME+" VARCHAR(30),"
                    +COL_ADDRESS+" VARCHAR(100),"
                    +COL_CONTACT+" VARCHAR(30)"
                    + ")";

    public Sql(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        if(context instanceof MainActivity)
            activity = (MainActivity) context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if  exists " +TABLE_NAME);
        onCreate(sqLiteDatabase);

    }

    public long addContact(Contact contact){

        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_NAME,contact.getName());
        contentValues.put(COL_ADDRESS,contact.getAddress());
        contentValues.put(COL_CONTACT,contact.getContact());
        long id = db.insert(TABLE_NAME,null,contentValues);
        db.close();
        return id;
    }
    public Contact getContact(long id){

        SQLiteDatabase db  = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME,
                new String[]{COL_ID,COL_NAME,COL_ADDRESS,COL_CONTACT},
                COL_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor!=null)
            cursor.moveToFirst();
        else return null;
        Contact contact = new Contact();
        contact.setId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
        contact.setAddress(cursor.getString(cursor.getColumnIndex(COL_ADDRESS)));
        contact.setContact(cursor.getString(cursor.getColumnIndex(COL_CONTACT)));
        contact.setContact(cursor.getString(cursor.getColumnIndex(COL_CONTACT)));
        db.close();
        return contact;

    }
    public long updateContact(Contact contact){

        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_NAME,contact.getName());
        contentValues.put(COL_ADDRESS,contact.getAddress());
        contentValues.put(COL_CONTACT,contact.getContact());
        long id = db.update(TABLE_NAME,contentValues,COL_ID + " = ? ",new String[]{String.valueOf(contact.getId())});
        db.close();
        Toast.makeText(activity,"Updated",Toast.LENGTH_LONG).show();
        return id;
    }

    public void deleteContact(Contact contact){

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME,COL_ID+" = ? ",new String[]{String.valueOf(contact.getId())});
        db.close();
        Toast.makeText(activity,"Deleted",Toast.LENGTH_LONG).show();

    }

    public List<Contact> getAllContacts(){
        List<Contact> contacts = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor !=null)
            if(cursor.moveToFirst()){
            do{
                Contact contact = new Contact();
                contact.setId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                contact.setAddress(cursor.getString(cursor.getColumnIndex(COL_ADDRESS)));
                contact.setName(cursor.getString(cursor.getColumnIndex(COL_NAME)));
                contact.setContact(cursor.getString(cursor.getColumnIndex(COL_CONTACT)));
                contacts.add(contact);

            }while (cursor.moveToNext());
            }
            db.close();
            return contacts;
    }
}

