package stimulation.contact.contactstimulation;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import stimulation.contact.contactstimulation.DataBase.Sql;
import stimulation.contact.contactstimulation.Fragments.Call;
import stimulation.contact.contactstimulation.Fragments.ContactList;
import stimulation.contact.contactstimulation.Fragments.Dialer;

public class MainActivity extends AppCompatActivity {

    public Sql database;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Dialer dialer;
    private ContactList contactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        database = new Sql(this);  // initialization database
        this.dialer = new Dialer();       // phone dialer fragment
        this.contactList = new ContactList();  // save phone contact list
        viewPager =  findViewById(R.id.viewpager);  // view pager, displaying both fragment
        setupViewPager(viewPager);
        tabLayout =  findViewById(R.id.tabs);      // tabs
        tabLayout.setupWithViewPager(viewPager);
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(dialer, "Dialer");
        adapter.addFragment(contactList, "Contacts");
        viewPager.setAdapter(adapter);
    }
    public void dialerClick(View view){
        dialer.dialerClick(view);

    }
    public void back(View view){
        dialer.back(view);

    }
    public void updateDb(){
        contactList.updateData();
    }
    public void call(View view){  // calling fake screen
        startActivity(new Intent(this,CallActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

}
