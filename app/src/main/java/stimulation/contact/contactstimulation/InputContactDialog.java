package stimulation.contact.contactstimulation;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class InputContactDialog extends Dialog implements View.OnClickListener {
    private EditText name,contact,address;
    private Button cancel,add;
    private MainActivity activity;
    private Contact editContact;

    public InputContactDialog(@NonNull Context context,Contact contact) {
        super(context);
        if(context instanceof MainActivity)
            activity = (MainActivity) context;
        this.editContact = contact;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_new_contact);
        findViewByIds();
        init();
    }

    private void findViewByIds() {
        name = findViewById(R.id.name);
        contact = findViewById(R.id.contact);
        address = findViewById(R.id.address);
        cancel = findViewById(R.id.cancel);
        add = findViewById(R.id.add);
    }
    private void init(){
        cancel.setOnClickListener(this);
        add.setOnClickListener(this);
        if(this.editContact != null){
            this.name.setText(editContact.getName());
            this.contact.setText(editContact.getContact());
            this.address.setText(editContact.getAddress());
            add.setText("Update");
        }
        else add.setText("Add");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add:
                Log.e("contact",contact.getText().toString());
                if(contact.getText().toString().length() == 11 &&Integer.parseInt(contact.getText().toString().substring(0,1))==0) {
                    if(editContact == null) {
                        Contact newContact = new Contact();
                        newContact.setContact(contact.getText().toString());
                        newContact.setAddress(address.getText().toString());
                        newContact.setName(name.getText().toString());
                        activity.database.addContact(newContact);

                    }
                    else {
                        editContact.setContact(contact.getText().toString());
                        editContact.setAddress(address.getText().toString());
                        editContact.setName(name.getText().toString());
                        activity.database.updateContact(editContact);
                    }
                    activity.updateDb();
                    clear();
                    dismiss();
                }
                else Toast.makeText(activity,"Contact length must be 11 and should start with 0",Toast.LENGTH_LONG).show();
            break;
            case R.id.cancel:
                dismiss();
                break;
        }

    }

    private void clear() {
        this.name.setText("");
        this.contact.setText("");
        this.address.setText("");
    }

}
