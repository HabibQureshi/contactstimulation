package stimulation.contact.contactstimulation.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import stimulation.contact.contactstimulation.MainActivity;
import stimulation.contact.contactstimulation.R;


public class Dialer extends Fragment {
    private View layout;
    private MainActivity activity;
    private TextView dialerNumber;
    public Dialer() {
        super();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.layout_dialer, container, false);
        findByIds();
        return layout;
    }

    private void findByIds() {
        dialerNumber = layout.findViewById(R.id.dialerNumber);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof MainActivity)
            activity = (MainActivity) context;
    }

    public void dialerClick(View view) { //displaying dialed number on screen
        verifyAndSetTextToDialer(((TextView)view).getText().toString());
    }

    private void verifyAndSetTextToDialer(String s) {
        String dialerText = dialerNumber.getText().toString();

        if(dialerText.length() == 0 && Integer.parseInt(s) != 0) {
            Toast.makeText(activity, "Number can only start with '0'", Toast.LENGTH_LONG).show();
        }
        else dialerNumber.setText(dialerNumber.getText().toString()+s);

    }

    public void back(View view) {
        String dialerText = dialerNumber.getText().toString();
        if(dialerText.length()>0){
            dialerNumber.setText(dialerText.substring(0,dialerText.length()-1));
        }
    }
}
