package stimulation.contact.contactstimulation.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import stimulation.contact.contactstimulation.CallActivity;
import stimulation.contact.contactstimulation.MainActivity;
import stimulation.contact.contactstimulation.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Call extends Fragment implements View.OnClickListener{
    private String number;
    private TextView num;
    private Button cancelCall;
    private View layout;
    private CallActivity activity;


    public void setNumber(String number) {
        this.number = number;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CallActivity)
            activity = (CallActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        layout =  inflater.inflate(R.layout.fragment_call, container, false);
        findViews();
        init();
        return layout;
    }

    private void init() {
        cancelCall.setOnClickListener(this);
    }

    private void findViews() {
        cancelCall = layout.findViewById(R.id.cancelCall);
    }

    @Override
    public void onClick(View view) {
        activity.cancelCall();

    }
}
