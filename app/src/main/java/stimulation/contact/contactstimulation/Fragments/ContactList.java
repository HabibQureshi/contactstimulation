package stimulation.contact.contactstimulation.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import stimulation.contact.contactstimulation.ContactListAdapter;
import stimulation.contact.contactstimulation.InputContactDialog;
import stimulation.contact.contactstimulation.MainActivity;
import stimulation.contact.contactstimulation.R;


public class ContactList extends Fragment implements View.OnClickListener {
    private RecyclerView recyclerView;
    private View layout;
    private MainActivity activity;
    private ContactListAdapter contactListAdapter;
    private Button addContact;
    private InputContactDialog inputContactDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.layout_contact_list, container, false);
        findByIds();
        init();
        return layout;
    }

    private void init() {
        contactListAdapter = new ContactListAdapter(activity);
        contactListAdapter.setContacts(activity.database.getAllContacts());
        recyclerView.setAdapter(contactListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        addContact.setOnClickListener(this);
        inputContactDialog = new InputContactDialog(activity,null);
        inputContactDialog.setCanceledOnTouchOutside(false);
    }

    private void findByIds() {
        recyclerView = layout.findViewById(R.id.contactList);
        addContact = layout.findViewById(R.id.addContact);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof MainActivity)
            activity = (MainActivity) context;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addContact:
                inputContactDialog.show();
                break;

        }

    }
    public void updateData(){

        contactListAdapter.updateData(activity.database.getAllContacts());
        contactListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(inputContactDialog !=null && inputContactDialog.isShowing())
                inputContactDialog.cancel();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(inputContactDialog !=null && inputContactDialog.isShowing())
            inputContactDialog.cancel();
    }
}
