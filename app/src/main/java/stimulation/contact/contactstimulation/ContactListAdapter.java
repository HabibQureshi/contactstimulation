package stimulation.contact.contactstimulation;

import android.content.Context;
import android.os.LocaleList;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {

    private View layout;
    private LayoutInflater inflater;
    private List<Contact> contacts;
    private ContactLongClickDailog contactLongClickDailog;
    private MainActivity activity;

    public ContactListAdapter(Context context) {
        if(context instanceof MainActivity)
            this.activity = (MainActivity) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        layout = inflater.inflate(R.layout.layout_contact, parent, false);
        ViewHolder holder = new ViewHolder(layout);

        return holder;
    }




    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {  // showing contacts information on screen
        holder.name.setText(contacts.get(position).getName());
        holder.address.setText(contacts.get(position).getAddress());
        holder.contact.setText(contacts.get(position).getContact());
        holder.id.setText(contacts.get(position).getId().toString());

    }

    @Override
    public int getItemCount() {  // total saved contacts
         if(contacts!=null)
            return contacts.size();
         else return 0;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }  // set contact list



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener {
        TextView name, address, contact,id;
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);  // click function
            itemView.setOnLongClickListener(this); //long click function
            name =  itemView.findViewById(R.id.name);
            address = itemView.findViewById(R.id.address);
            contact = itemView.findViewById(R.id.contact);
            id = itemView.findViewById(R.id.id);
        }
        @Override
        public void onClick(View view) { // call contact on click
            activity.call(view);



        }

        @Override
        public boolean onLongClick(View view) {  // EDIT data on long click
            contactLongClickDailog = new ContactLongClickDailog(activity,contacts.get(getAdapterPosition()));
            contactLongClickDailog.show();
            return true;
        }
    }
    public void updateData(List<Contact> contacts){
        this.contacts = contacts;
    }
}
