package stimulation.contact.contactstimulation;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;


public class ContactLongClickDailog extends Dialog implements View.OnClickListener {
    private Button delete,edit;
    private MainActivity activity;
    private InputContactDialog inputContactDialog;
    private Contact contact;


    public ContactLongClickDailog(@NonNull Context context,Contact contact) {
        super(context);
        if(context instanceof MainActivity)
            activity = (MainActivity) context;
        this.contact = contact;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_option);
        findViewByIds();
        init();
    }

    private void findViewByIds() {  // edit and delete options on long click
        delete = findViewById(R.id.delete);
        edit = findViewById(R.id.edit);

    }
    private void init(){
        edit.setOnClickListener(this);
        delete.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.edit:  //edit contact
                dismiss();
                inputContactDialog = new InputContactDialog(activity, contact);
                inputContactDialog.show();
            break;
            case R.id.delete: //delete contact and update database
                activity.database.deleteContact(contact);
                activity.updateDb();
                dismiss();
                break;
        }

    }

    public void setContact(Contact contact){
        this.contact = contact;
    }

    @Override
    public void cancel() {
        inputContactDialog = null;
        super.cancel();
    }
}
